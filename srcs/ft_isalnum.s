global _ft_isalnum

section .text
	extern _ft_isalpha
	extern _ft_isdigit

_ft_isalnum:
	call _ft_isdigit
	cmp rax, 0
	jg _true
	call _ft_isalpha
	cmp rax, 0
	jg _true
	je _false

_true:
	mov rax, 1
	ret

_false:
	mov rax, 0
	ret