global _ft_strcat

section .text
	extern _ft_strlen

_ft_strcat:
	mov rbx, rdi
	call _ft_strlen
	
_loop:
	cmp byte[rsi], 0
	jle _end
	mov r12, [rsi]
	mov [rbx+rax], r12
	inc rax
	inc rsi
	jmp _loop

_end:
	mov byte[rbx+rax], 0
	mov rax, rbx
	ret