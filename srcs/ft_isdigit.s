global _ft_isdigit

_ft_isdigit:
	cmp rdi, 48
	jl _false
	cmp rdi, 57
	jle _true
	jge _false

_true:
	mov rax, 1
	ret

_false:
	mov rax, 0
	ret