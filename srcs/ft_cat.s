global _ft_cat

section .text
	extern _ft_strlen

_ft_cat:
	mov r12, rdi
	lea rsi, [rel buf]

_loop_read:
	mov rdi, r12
	mov rdx, 1
	mov rax, 0x2000003
	syscall
	jc _end ; why use jc ? 
	cmp rax, 0
	je _end
	mov rdi, 1
	lea rsi, [rel buf]
	mov rdx, 1
	mov rax, 0x2000004
	syscall
	jmp _loop_read

_end:
	ret

section .bss ; don"t understant why it works at the end of the file but not at the beginning
	buf: resb 16
