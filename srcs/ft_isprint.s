global _ft_isprint

_ft_isprint:
	cmp rdi, 32
	jb _false
	cmp rdi, 126
	jg _false
	jmp _true


_true:
	mov rax, 1
	ret

_false:
	mov rax, 0
	ret