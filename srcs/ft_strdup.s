global _ft_strdup

section .text
    extern _ft_strlen
    extern _malloc

_ft_strdup:
    mov rbx, rdi
    call _ft_strlen
    inc rax
    mov r12, rax
    mov rdi, rax
    push rcx ; for stack alignement -- but why does it work with this register ?
    call _malloc
    pop rcx ; for stack alignement

_copy:
    mov rdi, rax
    mov rsi, rbx
    mov r12, rcx
    cld
    rep movsb

_end:
    ret