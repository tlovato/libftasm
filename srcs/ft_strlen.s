global _ft_strlen

_ft_strlen:
	mov rcx, -1
	mov al, 0
	cld
	repne scasb
	not rcx
	sub rcx, 1

_end:
	mov rax, rcx
	ret
