global _ft_puts

section .text
	extern _ft_strlen

_ft_puts:
	mov r12, rdi
	call _ft_strlen
	mov rdx, rax
	mov rsi, r12
	mov rdi, 1
	mov rax, 0x2000004
	syscall

_newline:
	mov rdi, 1
	mov rdx, 1
	mov rax, 0x2000004
	lea rsi, [rel nl]
	syscall

_end:
	ret

; why does it works when I put the .data section at the end but not when I put it at the beggining?
section .data
	nl db 10