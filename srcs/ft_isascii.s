global _ft_isascii

_ft_isascii:
	cmp rdi, 0
	jb _false
	cmp rdi, 127
	jg _false
	jmp _true

_true:
	mov rax, 1
	ret

_false:
	mov rax, 0
	ret