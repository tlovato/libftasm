global _ft_tolower

_ft_tolower:
	cmp rdi, 65
	jl _end
	cmp rdi, 90
	jg _end
	add rdi, 32

_end:
	mov rax, rdi
	ret