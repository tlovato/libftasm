global _ft_memset

_ft_memset:
	mov rbx, rdi
	mov rcx, rdx
	mov rax, rsi
	cld
	rep stosb

_end: 
	mov rax, rbx
	ret