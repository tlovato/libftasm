global _ft_isalpha

_ft_isalpha:
	cmp rdi, 64
	jle _false
	cmp rdi, 90
	jle _true
	cmp rdi, 96
	jle _false
	cmp rdi, 122
	jle _true
	jge _false

_true:
	mov rax, 1
	ret

_false:
	mov rax, 0
	ret
