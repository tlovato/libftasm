global _ft_strjoin

section .text
	extern _ft_strlen
	extern _malloc
	extern _ft_memcpy


_ft_strjoin:
	push rbp
	mov rbp, rsp
	push rsi ; s2 on stack
	push rdi ; s1 on stack
	call _ft_strlen
	push rax ; len s1 on stack
	mov rdi, rsi
	call _ft_strlen
	pop rdi ; recup len s1
	add rax, rdi ; len s2 + len s1
	inc rax
	mov rdi, rax
	call _malloc

	pop rdi ; recup s1
	push rdi ; s1 on stack
	push rax ; ptr ret malloc on stack
	call _ft_strlen
	pop rdi ; recup ptr malloc
	pop rsi ; recup s1
	pop rdx ; recup s2
	push rsi ; s1 on stack
	push rdx ; s2 on stack
	push rax ; len s1 on stack
	mov rdx, rax
	call _ft_memcpy
	pop rcx ; recup len s1
	pop rsi ; recup s2

_loop_s2:
	cmp byte[rsi], 0
	je _end
	mov r12, [rsi]
	mov [rax+rcx], r12
	inc rsi
	inc rcx
	jmp _loop_s2

_end:
	mov byte[rax+rcx], 0
	leave
	ret