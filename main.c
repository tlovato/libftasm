/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Pyxis <Pyxis@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/16 19:41:16 by Pyxis             #+#    #+#             */
/*   Updated: 2018/01/25 00:59:05 by Pyxis            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "incs/libftasm.h"
#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>

/* ************** DISPLAY RESULT ************* */
static void			display_result_str(char *str1, char *str2)
{
	if (!strcmp(str1, str2))
		printf("\033[1;32mvalide\033[m\n");
	else
		printf("\033[1;33merreur\033[m\n");
	return;
}

static void			display_result_int(int n1, int n2)
{
	if (n1 == n2)
		printf("\033[1;32mvalide\033[m\n");
	else
		printf("\033[1;33merreur\033[m\n");
	return ;
}

/* **************** FT_BZERO ****************** */

static void			test_ft_bzero(int n, char *str)
{
	char			*to_cmp;

	printf("\"%s\", %d : ", str, n);
	to_cmp = strdup(str);
	str = strdup(str);
	bzero(to_cmp, n);
	ft_bzero(str, n);
	display_result_str(to_cmp, str);
	printf("\tbzero : \"%s\"\n\tft_bzero : \"%s\"\n\n", to_cmp, str);
	free(to_cmp);
	free(str);
	return;
}

static void liste_ft_bzero(void)
{
	printf("\033[1;35mTESTS FT_BZERO :\033[m\n");
	test_ft_bzero(0, "This is a string");
	test_ft_bzero(1, "This is a string");
	test_ft_bzero(2, "42 is a number");
	test_ft_bzero(0, "");
	printf("\n");
	return;
}

/* ****************** FT_STRCAT **************** */

static void			test_ft_strcat(char *s1, char *s2)
{
	char			to_cmp[100];
	char			result[100];

	printf("\"%s\", \"%s\" : ", s1, s2);
	strcpy(to_cmp, s1);
	strcpy(result, s1);
	strcat(to_cmp, s2);
	ft_strcat(result, s2);
	display_result_str(to_cmp, result);
	printf("\tstrcat : \"%s\"\n\tft_strcat : \"%s\"\n\n", to_cmp, result);
	return;
}


static void			liste_ft_strcat(void)
{
	printf("\033[1;35mTESTS FT_STRCAT :\033[m\n");
	test_ft_strcat("this is ", "a string");
	test_ft_strcat("", "first empty string");
	test_ft_strcat("second empty string", "");
	test_ft_strcat("", "");
	printf("\n");
	return;
}

/* *************** FT_STRLEN ******************* */

static void			test_ft_strlen(char *str)
{
	int				n1;
	int				n2;
	
	printf("\"%s\" : ", str);
	n1 = strlen(str);
	n2 = ft_strlen(str);
	display_result_int(n1, n2);
	printf("\tstrlen : %d\n\tft_strlen : %d\n\n", n1, n2);
	return;
}

static void			liste_ft_strlen(void)
{
	printf("\033[1;35mTESTS FT_STRLEN :\033[m\n");
	test_ft_strlen("");
	test_ft_strlen("42");
	test_ft_strlen("Montpellier, first mentioned in a document of 985, was founded under a local feudal dynasty, the Guilhem, who combined two hamlets and built a castle and walls around the united settlement. The two surviving towers of the city walls, the Tour des Pins and the Tour de la Babotte, were built later, around the year 1200.");
	printf("\n");
	return;
}

/* **************** FT_ISALPHA ***************** */

static void			test_ft_isalpha(int c)
{
	int				ret1;
	int				ret2;

	printf("%d : ", c);
	ret1 = isalpha(c);
	ret2 = ft_isalpha(c);
	display_result_int(ret1, ret2);
	printf("\tisalpha : %d\n\tft_isalpha : %d\n", ret1, ret2);
	return;
}

static void			test_ft_isdigit(int c)
{
	int				ret1;
	int				ret2;

	printf("%d : ", c);
	ret1 = isdigit(c);
	ret2 = ft_isdigit(c);
	display_result_int(ret1, ret2);
	printf("\tisdigit : %d\n\tft_isdigit : %d\n", ret1, ret2);
	return;
}

static void			test_ft_isalnum(int c)
{
	int				ret1;
	int				ret2;

	printf("%d : ", c);
	ret1 = isalnum(c);
	ret2 = ft_isalnum(c);
	display_result_int(ret1, ret2);
	printf("\tisalnum : %d\n\tft_isalnum : %d\n", ret1, ret2);
	return;
}

static void			test_ft_isascii(int c)
{
	int				ret1;
	int				ret2;

	printf("%d : ", c);
	ret1 = isascii(c);
	ret2 = ft_isascii(c);
	display_result_int(ret1, ret2);
	printf("\tisascii : %d\n\tft_isascii : %d\n", ret1, ret2);
	return;
}

static void			test_ft_isprint(int c)
{
	int				ret1;
	int				ret2;

	printf("%d : ", c);
	ret1 = isprint(c);
	ret2 = ft_isprint(c);
	display_result_int(ret1, ret2);
	printf("\tisprint : %d\n\tft_isprint : %d\n", ret1, ret2);
	return;
}

static void			liste_ft_is(char *str)
{
	int				i;

	i = 0;
	printf("\033[1;35mTESTS FT_IS%s :\033[m\n", str);
	while (i <= 127)
	{
		if (!strcmp(str, "ALPHA"))
			test_ft_isalpha(i);
		if (!strcmp(str, "DIGIT"))
			test_ft_isdigit(i);
		if (!strcmp(str, "ALNUM"))
			test_ft_isalnum(i);
		if (!strcmp(str, "ASCII"))
			test_ft_isascii(i);
		if (!strcmp(str, "PRINT"))
			test_ft_isprint(i);
		i++;
	}
	printf("\n");
	return;
}

/* ***************** FT_TOUPPER **************** */

static void			test_ft_toupper(int c)
{
	int				ret1;
	int				ret2;

	printf("'%c' : ", c);
	ret1 = toupper(c);
	ret2 = ft_toupper(c);
	display_result_int(ret1, ret2);
	printf("\ttoupper : %c\n\tft_toupper : %c\n", ret1, ret2);
}

static void			liste_ft_toupper(void)
{
	int				i;

	i = 97;
	printf("\033[1;35mTESTS FT_TOUPPER :\033[m\n");
	while (i <= 122)
	{
		test_ft_toupper(i);
		i++;
	}
	test_ft_toupper(53);
	test_ft_toupper(42);
	printf("\n");
	return;
}

/* ****************** FT_TOLOWER *************** */

static void			test_ft_tolower(int c)
{
	int				ret1;
	int				ret2;

	printf("'%c' : ", c);
	ret1 = tolower(c);
	ret2 = ft_tolower(c);
	display_result_int(ret1, ret2);
	printf("\ttolower : %c\n\tft_tolower : %c\n", ret1, ret2);
}

static void			liste_ft_tolower(void)
{
	int				i;

	i = 65;
	printf("\033[1;35mTESTS FT_TOLOWER :\033[m\n");
	while (i <= 90)
	{
		test_ft_tolower(i);
		i++;
	}
	test_ft_tolower(53);
	test_ft_tolower(42);
	printf("\n");
	return;
}

/* ****************** FT_PUTS ***************** */

static void			test_ft_puts(char *str)
{
	printf("\"%s\" : \n", str);
	ft_puts(str);
	printf("\n");
	return ;
}

static void			liste_ft_puts(void)
{
	printf("\033[1;35mTESTS FT_PUTS :\033[m\n");
	test_ft_puts("");
	test_ft_puts("bonjour");
	test_ft_puts("Montpellier, first mentioned in a document of 985, was founded under a local feudal dynasty, the Guilhem, who combined two hamlets and built a castle and walls around the united settlement. The two surviving towers of the city walls, the Tour des Pins and the Tour de la Babotte, were built later, around the year 1200.");
	printf("\n");
	return;
}

/* ***************** FT_MEMSET ***************** */

static void			test_ft_memset(char *s, int c, size_t len)
{
	char			*str;
	char			*to_cmp;
	char			*res;

	printf("\"%s\", %c, %zu : ", s, c, len);
	str = strdup(s);
	to_cmp = memset(str, c, len);
	res = ft_memset(str, c, len);
	display_result_str(to_cmp, res);
	printf("\tmemset : %s\n\tft_memset : %s\n\n", to_cmp, res);
	free(str);
	return;
}

static void			liste_ft_memset(void)
{
	printf("\033[1;35mTESTS FT_MEMSET :\033[m\n");
	test_ft_memset("salut camarade", 42, 5);
	test_ft_memset("", 42, 8);
	test_ft_memset("this is a string", 53, 0);
	test_ft_memset("this is a string", 0, 10);
	printf("\n");
	return;
}

/* ***************** FT_MEMCPY ***************** */

static void			test_ft_memcpy(char *s1, char *s2, size_t len)
{
	char			*to_cmp;
	char			*res;

	printf("\"%s\", \"%s\", %zu : ", s1, s2, len);
	to_cmp = memcpy(s1, s2, len);
	res = ft_memcpy(s1, s2, len);
	display_result_str(to_cmp, res);
	printf("\tmemcpy : %s\n\tft_memcpy : %s\n\n", to_cmp, res);
	bzero(s1, 500);
	return ;
}

static void			liste_ft_memcpy(void)
{
	char			s1[500];

	bzero(s1, 500);
	printf("\033[1;35mTESTS FT_MEMCPY :\033[m\n");
	test_ft_memcpy(s1, "This is a string", 16);
	test_ft_memcpy(s1, "This is a string", 10);
	test_ft_memcpy(s1, "Montpellier, first mentioned in a document of 985, was founded under a local feudal dynasty, the Guilhem, who combined two hamlets and built a castle and walls around the united settlement. The two surviving towers of the city walls, the Tour des Pins and the Tour de la Babotte, were built later, around the year 1200.", 319);
	test_ft_memcpy(s1, "42", 2);
	printf("\n");
	return;
}

/* ****************** FT_STRDUP **************** */

static void			test_ft_strdup(char *str)
{
	char			*to_cmp;
	char			*res;

	printf("\"%s\" : ", str);
	to_cmp = strdup(str);
	res = ft_strdup(str);
	display_result_str(to_cmp, res);
	printf("\tstrdup : %s\n\tft_strdup : %s\n\n", to_cmp, res);
	return ;
}

static void			liste_ft_strdup(void)
{
	printf("\033[1;35mTESTS FT_STRDUP :\033[m\n");
	test_ft_strdup("");
	test_ft_strdup("42");
	test_ft_strdup("Montpellier, first mentioned in a document of 985, was founded under a local feudal dynasty, the Guilhem, who combined two hamlets and built a castle and walls around the united settlement. The two surviving towers of the city walls, the Tour des Pins and the Tour de la Babotte, were built later, around the year 1200.");
	printf("\n");
	return;
}

/* ************** FT_STRJOIN ************** */

static void			test_ft_strjoin(char *s1, char *s2)
{
	char			*res;

	printf("\"%s\", \"%s\" : \n", s1, s2);
	res = ft_strjoin(s1, s2);
	printf("%s\n\n", res);
	free(res);
	return;
}

static void			liste_ft_strjoin(void)
{
	printf("\033[1;35mTESTS FT_STRJOIN :\033[m\n");
	test_ft_strjoin("bonjour ", "camarade");
	test_ft_strjoin("", "first empty string");
	test_ft_strjoin("second empty string", "");
	test_ft_strjoin("Montpellier, first mentioned in a document of 985, was founded under a local ", "feudal dynasty, the Guilhem, who combined two hamlets and built a castle and walls around the united settlement. The two surviving towers of the city walls, the Tour des Pins and the Tour de la Babotte, were built later, around the year 1200.");
	printf("\n");
}

/* ***************** FT_CAT ******************** */

static void			test_ft_cat(int fd)
{
	printf("%d : \n", fd);
	ft_cat(fd);
	printf("\n");
	return;
}

static void			liste_ft_cat(void)
{
	int				fd;

	fd = open("srcs/ft_strjoin.s", O_RDONLY);
	printf("\033[1;35mTESTS FT_CAT :\033[m\n");
	test_ft_cat(fd);
	test_ft_cat(125);
	test_ft_cat(-1);
	close(fd);
	test_ft_cat(fd);
	test_ft_cat(0);
	printf("\n");
}

/* ****************** MAIN ********************* */

int					main(void)
{	
	liste_ft_bzero();
	liste_ft_strcat();
	liste_ft_strlen();
	liste_ft_is("ALPHA");
	liste_ft_is("DIGIT");
	liste_ft_is("ALNUM");
	liste_ft_is("ASCII");
	liste_ft_is("PRINT");
	liste_ft_toupper();
	liste_ft_tolower();
	liste_ft_puts();
	liste_ft_memset();
	liste_ft_memcpy();
	liste_ft_strdup();
	liste_ft_strjoin();
	liste_ft_cat();
	return (0);
}


