# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: Pyxis <Pyxis@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/15 23:18:36 by Pyxis             #+#    #+#              #
#    Updated: 2018/07/09 19:14:46 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libfts.a
TEST = main_test

INCS_DIR = incs/
SRCS_DIR = srcs/

SRCS_FILES = ft_bzero.s ft_strcat.s ft_strlen.s ft_isalpha.s ft_isdigit.s\
	ft_isalnum.s ft_isascii.s ft_isprint.s ft_toupper.s ft_tolower.s ft_puts.s\
	ft_memset.s ft_memcpy.s ft_strdup.s ft_cat.s ft_strjoin.s

OBJS = $(addprefix $(SRCS_DIR), $(SRCS_FILES:.s=.o))

all : $(NAME) $(TEST)

$(NAME) : $(OBJS)
		@ar rc $(NAME) $(OBJS)
		@ranlib $(NAME)
		@echo "\n\033[1;33m libfts.a : \tDONE ! \033[m"

$(SRCS_DIR)%.o: $(SRCS_DIR)%.s
		@nasm -f macho64 -o $@ $<
		@echo "\033[1;36m compil $< : \tDONE ! \033[m"

$(TEST) : $(NAME) main.c
	@gcc -I $(INCS_DIR) -L . -lfts main.c -o $@ -g3
	@echo "\033[1;35m main_test : \tDONE ! \033[m"

clean :
		@rm -f $(OBJS)
		@echo "\033[1;32m clean : \tDONE ! \033[m"

fclean : clean
		@rm -f $(NAME) main_test
		@echo "\033[1;32m fclean : \tDONE ! \033[m\n"

re : fclean all
